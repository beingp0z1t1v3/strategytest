package org.java.patterns.strategy;

import org.java.patterns.strategy.abstractions.Duck;
import org.java.patterns.strategy.ducks.DefaultDuck;
import org.java.patterns.strategy.ducks.ToyDuck;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Duck duck1 = new DefaultDuck();
        Duck duck2 = new ToyDuck();

        List<Duck> ducks = new ArrayList<>();
        ducks.add(duck1);
        ducks.add(duck2);
        ducks.forEach(duck -> {
            duck.fly(duck.getFlyMethod());
            duck.quack(duck.getQuackMethod());
        });
    }
}
