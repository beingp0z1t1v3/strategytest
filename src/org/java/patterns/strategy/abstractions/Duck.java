package org.java.patterns.strategy.abstractions;

import org.java.patterns.strategy.interfaces.FlyBehavior;
import org.java.patterns.strategy.interfaces.QuackBehavior;

public abstract class Duck implements FlyBehavior, QuackBehavior {

    private String flyMethod;
    private String quackMethod;


    public Duck (String flyMethod, String quackMethod) {
        this.flyMethod = flyMethod;
        this.quackMethod = quackMethod;
    }

    public String getFlyMethod() {
        return flyMethod;
    }

    public void setFlyMethod(String flyMethod) {
        this.flyMethod = flyMethod;
    }

    public String getQuackMethod() {
        return quackMethod;
    }

    public void setQuackMethod(String quackMethod) {
        this.quackMethod = quackMethod;
    }
}
