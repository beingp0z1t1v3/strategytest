package org.java.patterns.strategy.interfaces;

public interface FlyBehavior {
    public default void fly(String flyMethod) {
        System.out.println(flyMethod);
    };

}
