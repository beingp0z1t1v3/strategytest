package org.java.patterns.strategy.interfaces;

public interface QuackBehavior {
    public default void quack(String quackMethod) {
        System.out.println(quackMethod);
    };
}
