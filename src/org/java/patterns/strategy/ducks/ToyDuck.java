package org.java.patterns.strategy.ducks;

import org.java.patterns.strategy.abstractions.Duck;

public class ToyDuck extends Duck {
    public ToyDuck() {
        super("Nothing", "RoboVoice");
    }
}
