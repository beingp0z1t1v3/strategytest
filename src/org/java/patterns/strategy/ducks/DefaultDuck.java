package org.java.patterns.strategy.ducks;

import org.java.patterns.strategy.abstractions.Duck;

public class DefaultDuck extends Duck {
    public DefaultDuck() {
        super("Wings", "Loud");
    }
}
